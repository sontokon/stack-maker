using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class SoundManager : Singleton<SoundManager>
{
    [Header("Audio Source")]
    public AudioSource soundGame;
    public AudioSource soundThemeGame;
    [Header("Audio Clip")]
    public AudioClip brickSound;
    public AudioClip fireworkSound;
    public void ChangeVolumeGame(float volume)
    {
        soundGame.volume = volume;
        soundThemeGame.volume = volume;
    }    
    public void PlayFireWorkSound()
    {
        soundGame.PlayOneShot(fireworkSound);
    }    
    public void PlayBrickSound()
    {
        soundGame.PlayOneShot(brickSound);
    }    
}
