using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class PlayerAnim : Singleton<PlayerAnim>
{
    public Animator playerAnim;

    public void ChangeToAnimJump()
    {
        playerAnim.SetInteger("renwu", 1);     
    }
    public void ChangeToAnimIdle()
    {
        playerAnim.SetInteger("renwu", 0);
    }  
    public void ChangeToAnimWin()
    {
        playerAnim.SetInteger("playerstate", 2);
        print("run this anim");
    }    

}
