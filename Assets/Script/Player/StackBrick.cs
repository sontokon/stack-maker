using UnityEngine;

public class StackBrick : MonoBehaviour
{
    private bool canRemove = true;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Brickpickup")
        {
            var purpleCell = other.transform.parent.GetComponent<PurpleCell>();
            if (purpleCell.HasBrick())
            {
                StackController.instance.AddBrick(purpleCell.DetechBrick());
                GameManager.instance.IncreaseScore();
            }
            purpleCell.TryGetComponent<PushCell>(out var pushCell);
            if (pushCell)
            {
                Debug.Log(pushCell.ToString());
                PlayerMovement.instance.MovePlayerDirection(pushCell.GetDirectionOnEnterCell(PlayerMovement.instance.currentDirection));
            }
            //StackController.instance.AddBrick(other.gameObject);
            //other.gameObject.AddComponent<Rigidbody>();
            //other.gameObject.GetComponent<Rigidbody>().useGravity = false;
            //other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
            //other.gameObject.AddComponent<StackBrick>();
            //StackController.instance.brickList.Add(other.gameObject);
        }   
        if(other.tag == "RemoveBrick")
        {
            var bridge = other.GetComponent<Bridge>();
            bridge.OnPlayerEnter();
            //_ = StackController.instance.RemoveBrickToBridge();
            //StackController.instance.RemoveBrick();
            //foreach(GameObject brick in StackController.instance.brickList)
            //{
            //    brick.GetComponent<StackBrick>().canRemove = false;
            //}
            //print("remove brick");
        }

    }
    private void OnTriggerExit(Collider other)
    {
        //if( other.tag == "RemoveBrick")
        //{
        //    foreach (GameObject brick in StackController.instance.brickList)
        //    {
        //        brick.GetComponent<StackBrick>().canRemove = true;
        //    }
        //}
        if (other.tag == "Winpos")
        {
            StackController.instance.RemoveAllBrick();
            PlayerAnim.instance.ChangeToAnimWin();
            GameManager.instance.IsWinning();
            Level.instance.PlayConfetti();
            PlayerMovement.instance.StopPlayer();
        }
    }
}
