using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class StackController : Singleton<StackController>
{
    
    public List<GameObject> brickList = new List<GameObject>();
    public GameObject dashParent;
    public GameObject prevDash;
    public Transform man;
    private void Start()
    {
        brickList.Add(prevDash);    
    }
    public void AddBrick(GameObject brickobj)
    {
        brickobj.transform.SetParent(dashParent.transform);
        Vector3 pos = prevDash.transform.localPosition;
        pos.y -= 0.5f;
        brickobj.transform.localPosition = pos;

        Vector3 characterPos = man.localPosition;
        characterPos.y += 0.5f;
        man.localPosition = characterPos;

        Vector3 dashParentPos = dashParent.transform.localPosition;
        dashParentPos.y += 0.5f;
        dashParent.transform.localPosition = dashParentPos; 

        prevDash = brickobj;
        //prevDash.GetComponent<BoxCollider>().isTrigger = true;
        SoundManager.instance.PlayBrickSound();
        //DisableBrickCollider();
        brickList.Add(brickobj);
    }

    //private void DisableBrickCollider()
    //{
    //    foreach (GameObject brickobj in brickList)
    //    {
    //        brickobj.GetComponent<StackBrick>().enabled = false;
    //    }
    //}
    //public void RemoveBrick()
    //{
    //    if(brickList.Count >= 0) 
    //    {
    //        GameObject lastBrick = brickList[brickList.Count - 1];
    //        brickList.RemoveAt(brickList.Count - 1);
    //        Destroy(lastBrick);

    //        Vector3 characterPos = man.localPosition;
    //        characterPos.y -= 0.5f;
    //        man.localPosition = characterPos;

    //        Vector3 dashParentPos = dashParent.transform.localPosition;
    //        dashParentPos.y -= 0.5f;
    //        dashParent.transform.localPosition = dashParentPos;

    //        if (brickList.Count> 0)
    //        {
    //            prevDash = brickList[brickList.Count - 1];
    //        }         
    //    }
    //}

    public GameObject RemoveBrickToBridge(GameObject parent)
    {
        if (brickList.Count > 0)
        {
            GameObject lastBrick = brickList[brickList.Count - 1];
            brickList.RemoveAt(brickList.Count - 1);

            Vector3 characterPos = man.localPosition;
            characterPos.y -= 0.5f;
            man.localPosition = characterPos;

            Vector3 dashParentPos = dashParent.transform.localPosition;
            dashParentPos.y -= 0.5f;
            dashParent.transform.localPosition = dashParentPos;

            if (brickList.Count > 0)
            {
                prevDash = brickList[brickList.Count - 1];
            }
            if(dashParent!= null)
            {
                lastBrick.transform.parent = null;
                lastBrick.transform.SetParent(parent.transform);
            }    
            return lastBrick;
        }
        else
        {
            PlayerMovement.instance.StopPlayer();
            GameManager.instance.IsLosing();
            return null;
            //throw new ArgumentException("No more brick to remove");;
        }
        //throw new ArgumentException("No more brick to remove");
    }
    public void RemoveAllBrick()
    {
        while (brickList.Count > 0)
        {
            GameObject lastBrick = brickList[brickList.Count - 1];
            brickList.RemoveAt(brickList.Count - 1);
            Destroy(lastBrick);

            Vector3 characterPos = man.localPosition;
            characterPos.y -= 0.5f;
            man.localPosition = characterPos;

            if (brickList.Count > 0)
            {
                prevDash = brickList[brickList.Count - 1];
            }
        }
    }    
}
