using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Utils;


public enum Direction
{
    Up,
    Down,
    Right,
    Left,
    None
}
public class PlayerMovement : Singleton<PlayerMovement>
{
    private Vector2 startTouch;
    private Vector2 swipeDelta;
    private Vector3 previousPos;
    private const int deadZone = 100;
    private Rigidbody rb;
    private bool isMoving;

    public float rayDistance;
    public float moveSpeed;
    public LayerMask brickLayer;

    public Direction currentDirection {  get; private set; }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, rayDistance, brickLayer))
        {

            previousPos = hit.collider.transform.position;
        }
        else
        {
            transform.position = new Vector3(previousPos.x, transform.position.y, previousPos.z);
            rb.velocity = Vector3.zero;
        }
        if(rb.velocity == Vector3.zero)
        {
            isMoving = false;
        }
        OnScreenTouch();
    }
    public void OnScreenTouch()
    {
        if (Input.GetMouseButtonDown(0) && isMoving == false)
        {
            startTouch = Input.mousePosition;
            UIManager.instance.DeactiveTutorial();
        }
        else if (Input.GetMouseButtonUp(0) && isMoving == false)
        {
            swipeDelta = (Vector2)Input.mousePosition - startTouch;
            CheckSwipe();
            startTouch = swipeDelta = Vector2.zero;
        }
    }

    /// <summary>
    /// wherthe or not player swipe on the screen
    /// </summary>
    public void CheckSwipe()
    {
        if (swipeDelta.magnitude > deadZone)
        {
            isMoving = true;
            float x = swipeDelta.x;
            float y = swipeDelta.y;
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x < 0 && isMoving == true)
                {
                    MovePlayerDirection(Direction.Left);
                }
                else
                {
                    MovePlayerDirection(Direction.Right);
                }
            }
            else
            {
                if (y < 0 && isMoving == true)
                {
                    MovePlayerDirection(Direction.Down);
                }
                else
                {
                    MovePlayerDirection(Direction.Up);
                }
            }
        }
    }

    public void MovePlayerDirection(Direction direction)
    {
        Vector3 moveVector = Vector3.zero;
        currentDirection = direction;
        switch (direction)
        {
            case Direction.Left:
                moveVector = -transform.right;
                break;
            case Direction.Right:
                moveVector = transform.right;
                break;
            case Direction.Up:
                moveVector = transform.forward;
                break;
            case Direction.Down:
                moveVector = -transform.forward;
                break;
            case Direction.None:
                break;
        }
        PlayerAnim.instance.ChangeToAnimJump();
        rb.velocity = moveVector * moveSpeed;
        StartCoroutine(WaitForJump());
    }
    public void StopPlayer()
    {
        rb.velocity = Vector3.zero;
        moveSpeed = 0;
    }
    public void ResumePlayer()
    {
        moveSpeed = 25;
    }
    IEnumerator WaitForJump()
    {
        yield return new WaitForEndOfFrame();
        PlayerAnim.instance.ChangeToAnimIdle();
    }    

}
