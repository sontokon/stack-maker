using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class GameManager : Singleton<GameManager>
{
    private bool winningState = false;
    private bool losingState = false;

    private void Start()
    {
        Application.targetFrameRate = 60;
        UIManager.instance.ChangeScoreText(score);
    }
    public int score;
    public void IsWinning()
    {
        winningState = true;
        if(winningState == true)
        {
            print("you win!");
            UIManager.instance.OpenWinPanel();
            SoundManager.instance.PlayFireWorkSound();
        }
    } 
    public void IsLosing()
    {
        losingState = true;
        if(losingState == true)
        {
            print("you lose");
            UIManager.instance.OpenLosePanel();
        }
    }   
    public void IncreaseScore()
    {
        score++;
        UIManager.instance.ChangeScoreText(score);
    }    
}
