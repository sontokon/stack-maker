using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurpleCell : MonoBehaviour
{
    [SerializeField] private GameObject Brick;

    [SerializeField] private bool isHoldBrick = true;

    //private static readonly Vector3 brickPos = new Vector3(5.36f, 0, -1f);

    //public void AttachBrick(GameObject brick)
    //{
    //    Brick = brick;
    //    Brick.transform.position = brickPos;
    //    isHoldBrick = true;
    //}

    public GameObject DetechBrick()
    {
        isHoldBrick = false;
        var brick = Brick;
        Brick = null;
        return brick;
    }

    internal bool HasBrick()
    {
        return isHoldBrick;
    }
}
