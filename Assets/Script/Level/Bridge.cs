﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bridge : MonoBehaviour
{

    [SerializeField] private bool hasBrick;

    public GameObject brickPos;

     

    public void OnPlayerEnter()
    {
        if (hasBrick)
            OnHasBrick();
        else
            OnNoBrick();
    }

    private void OnNoBrick()
    {
        hasBrick = true;
        //Player Remove brick: StackController.RemoveBrickToBridge return GameObjec Brick.
        // Lấy cái brick trả về gắn vào thagnwf này 
        //StackController.instance.RemoveBrickToBridge().transform.SetParent(brickPos.transform);
        var brickObj = StackController.instance.RemoveBrickToBridge(brickPos);
        if(brickObj == null )
           return;
        brickObj.transform.position = brickPos.transform.position;
        //Bridge get 1 brick
    }

    private void OnHasBrick()
    {
        return;
    }
}
