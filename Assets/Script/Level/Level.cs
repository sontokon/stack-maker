using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class Level : Singleton<Level>
{
    public Transform playerPos;
    public Transform Player;
    public Transform startPos;
    public ParticleSystem confettiFireworks;
    private void Start()
    {
        playerPos.position =  startPos.position;
        CamController.instance.FollowPlayer(Player);
    }
    public void PlayConfetti()
    {
        confettiFireworks.Play();
    }
}
