using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushCell : MonoBehaviour
{
    public TwoWay TwoWay;

    public Direction GetDirectionOnEnterCell(Direction currentDirection)
    {
        return TwoWay switch
        {
            TwoWay.UpRight => UpRightCase(currentDirection),
            TwoWay.RightDown => RightDownCase(currentDirection),
            TwoWay.DownLeft => DownLeftCase(currentDirection),
            TwoWay.LeftUp => LeftUpCase(currentDirection),
            TwoWay.OnlyRight => OnlyRightCase(currentDirection),
            TwoWay.OnlyLeft => OnlyLeftCase(currentDirection),
            _ => Direction.None,
        };
    }

    private Direction UpRightCase(Direction currentDirection)
    {

        switch (currentDirection)
        {
            case Direction.Down:
                return Direction.Right;
            case Direction.Left:
                return Direction.Up;
            default:
                throw new ArgumentException($"current Direction must be Up or Right. Current is: {currentDirection}");
        }
    }
    private Direction RightDownCase(Direction currentDirection)
    {

        switch (currentDirection)
        {
            case Direction.Left:
                return Direction.Down;
            case Direction.Up:
                return Direction.Right;
            default:
                throw new ArgumentException($"current Direction must be Right or Down. Current is: {currentDirection}");
        }
    }

    private Direction DownLeftCase(Direction currentDirection)
    {

        switch (currentDirection)
        {
            case Direction.Up:
                return Direction.Left;
            case Direction.Right:
                return Direction.Down;
            default:
                throw new ArgumentException($"current Direction must be Down or Left. Current is: {currentDirection}");
        }
    }

    private Direction LeftUpCase(Direction currentDirection)
    {

        switch (currentDirection)
        {
            case Direction.Right:
                return Direction.Up;
            case Direction.Down:
                return Direction.Left;
            default:
                throw new ArgumentException($"current Direction must be Left or Up. Current is: {currentDirection}");
        }
    }
    private Direction OnlyRightCase(Direction currentDirection)
    {

        switch (currentDirection)
        {
            case Direction.Right:
                return Direction.Left;
            default:
                throw new ArgumentException($"current Direction must be Left or Up. Current is: {currentDirection}");
        }
    }
    private Direction OnlyLeftCase(Direction currentDirection)
    {

        switch (currentDirection)
        {
            case Direction.Left:
                return Direction.Right;
            default:
                throw new ArgumentException($"current Direction must be Left or Up. Current is: {currentDirection}");
        }
    }


}

public enum TwoWay
{
    UpRight,
    RightDown,
    DownLeft,
    LeftUp,
    OnlyRight,
    OnlyLeft
}