using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class LevelManager : Singleton<LevelManager>
{
    public List<GameObject> levelPrefab;
    public GameObject levelTemp;
    public int level;

    private void Start()
    {
        levelTemp = Instantiate(levelPrefab[level]);
        UIManager.instance.ChangeLevelText(level + 1);

    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            ChangeLevel();
        }    
    }
    public void RetryLevel()
    {
        if (levelPrefab[level] != null)
        {
            Destroy(levelTemp);
            Destroy(PlayerMovement.instance);
            Destroy(PlayerAnim.instance);
            Destroy(StackController.instance);
            Destroy(Level.instance);
            StartCoroutine(DelayLoadLevel());
            UIManager.instance.ChangeLevelText(level + 1);
        }
    }    
    public void ChangeLevel()
    {
        level++;
        if(level < levelPrefab.Count && levelPrefab[level]!= null)
        {
            Destroy(levelTemp);
            Destroy(PlayerMovement.instance);
            Destroy(PlayerAnim.instance);
            Destroy(StackController.instance);
            Destroy(Level.instance);
            StartCoroutine(DelayLoadLevel());
            UIManager.instance.ChangeLevelText(level + 1);
         }
        else
        {
            level = 0;
            Destroy(levelTemp);
            Destroy(PlayerMovement.instance);
            Destroy(PlayerAnim.instance);
            Destroy(StackController.instance);
            Destroy(Level.instance);
            StartCoroutine(DelayLoadLevel());
            UIManager.instance.ChangeLevelText(level+1);
        }    
    }
    IEnumerator DelayLoadLevel()
    {
        yield return new WaitForEndOfFrame();
        levelTemp = Instantiate(levelPrefab[level]);
    }
}
