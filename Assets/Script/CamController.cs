using Cinemachine;
using UnityEngine;
using Utils;

public class CamController : Singleton<CamController>
{
    private CinemachineVirtualCamera vCam;
    private void Awake()
    {
        vCam = GetComponent<CinemachineVirtualCamera>();
    }
    public void FollowPlayer(Transform playerPos)
    {
        vCam.Follow = playerPos;
    }    
}
