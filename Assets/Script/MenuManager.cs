using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public GameObject loadingPanel;
    public Slider loadingSlider;
    public void LoadScene(int sceneId)
    {
        StartCoroutine(LoadSceneAsync(sceneId));
    }   
    
    IEnumerator LoadSceneAsync(int sceneId)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneId);
        loadingPanel.SetActive(true);

        while(operation.isDone)
        {
            float progressValue = Mathf.Clamp01(operation.progress / 0.1f);
            loadingSlider.value = progressValue;
            yield return null;
        }
    }    
}
