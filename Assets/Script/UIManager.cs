
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class UIManager : Singleton<UIManager>
{
    [Header("Text")]
    public TMP_Text levelText;
    public TMP_Text scoreText;
    public TMP_Text fpsText;
    [Header("GameObject")]
    public GameObject winPanel;
    public GameObject losePanel;
    public GameObject settingPanel;
    public GameObject tutorial;
    [Header("Slider")]
    public Slider soundSlider;

    private int gameFps;
    private void Update()
    {
        gameFps = Application.targetFrameRate;
        SoundManager.instance.ChangeVolumeGame(soundSlider.value);
        fpsText.text = "fps: " + gameFps.ToString();
    }
    public void DeactiveTutorial()
    {
        tutorial.SetActive(false);
    }    
    public void OpenSetting()
    {
        settingPanel.SetActive(true);
        PlayerMovement.instance.StopPlayer();
    }   
    public void CloseSetting()
    {
        settingPanel.SetActive(false);
        PlayerMovement.instance.ResumePlayer();
    }    
    public void OnClickNextLevel()
    {
        winPanel.SetActive(false);
        LevelManager.instance.ChangeLevel();
    }    
    public void OnClickRetry()
    {
        winPanel.SetActive(false);
        losePanel.SetActive(false);
        LevelManager.instance.RetryLevel();
    }    
    public void ChangeLevelText(int level)
    {
        levelText.text = "level: " + level;
    }  
    public void ChangeScoreText(int text)
    {
        scoreText.text = "score: " + text;
    }    
    public void OpenWinPanel()
    {
        winPanel.SetActive(true);
    }   
    public void OpenLosePanel()
    {
        losePanel.SetActive(true);
    }    
}
